<?php require_once 'ExampleData.php' ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Listing</title>
    <script src="src/ListDotJs.js"></script>
    <script src="src/Components/ListDotJsComponent.ItemContainer.js"></script>
    <script src="src/Components/ListDotJsComponent.Item.js"></script>
    <script src="src/Plugins/ListDotJsPlugin.Selection.js"></script>
    <script src="src/Plugins/ListDotJsPlugin.Searching.js"></script>
    <script src="src/Plugins/ListDotJsPlugin.Sorting.js"></script>
    <script src="src/Plugins/ListDotJsPlugin.Filtering.js"></script>
    <script src="src/Plugins/ListDotJsPlugin.Pagination.js"></script>
    <script src="src/Plugins/ListDotJsPlugin.Effects.js"></script>
    <script src="src/Plugins/ListDotJsPlugin.Ajax.js"></script>

    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #eeeeee;
        }

        * {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        #actions {
            width: 960px;
            margin: 0 auto;
        }

        #list {
            width: 960px;
            margin: 0 auto;
            list-style: none;
        }

        .list-item {
            background-color: #fcfcfc;
            border-bottom: 1px solid #eee;
            width: 100%;
            overflow: hidden;
        }

        .list-item.selected {
            background-color: #9effa1 !important;
        }

        .list-item.odd {
            background-color: #eaeeff;
        }

        .list-item div {
            width: 50%;
            float: left;
        }

        .list-item div.right {
            float: right;
        }

        .pagination {
            clear:both;
            position:relative;
            font-size:11px;
            text-align:center;
        }

        .pagination span, .pagination li {
            display:inline-block;
            margin: 2px 2px 2px 0;
            padding:6px 9px 5px 9px;
            text-decoration:none;
            color: #ddd;
            background: #222;
        }

        .pagination .current{
            padding:6px 9px 5px 9px;
            font-weight: bold;
            background: #0950E8;
        }

    </style>
</head>
<body>


<div id="actions">
    <button onclick="list.getPlugin('SelectionMode').activate()">activateSelectionMode</button>
    <button onclick="list.getPlugin('SelectionMode').deactivate()">deactivateSelectionMode</button>

    <button onclick="list.trigger('showAll')">showAll</button>
    <button onclick="list.trigger('hideAll')">hideAll</button>
    <button onclick="list.trigger('selectAll')">selectAll</button>
    <button onclick="list.trigger('deselectAll')">deselectAll</button>
    <button onclick="list.trigger('removeSelected')">removeSelected</button>

    <button onclick="list.update()">update</button>

    <button class="filter" data-filter="all">clear Filter</button>
    <button class="filter" data-filter="category1">Filter by Category1</button>
    <button class="filter" data-filter="category2">Filter by Category2</button>

    <button class="sort" data-sort="id">Sort by Id ASC</button>
    <button class="sort" data-sort="id" data-order="desc">Sort by Id DESC</button>

    <button class="sort" data-sort="date">Sort by Date ASC</button>
    <button class="sort" data-sort="date" data-order="desc">Sort by Date DESC</button>

    <button class="sort" data-sort="name">Sort by Name ASC</button>
    <button class="sort" data-sort="name" data-order="desc">Sort by Name DESC</button>

    <input type="text" id="list-search" autofocus autocomplete>

    <div id="itemcount"></div>
    <div id="selectedItems"></div>
</div>

<div class="pagination"></div>

<ul id="list">
    <?php for ($i = 0; $i <= 300; $i++) : ?>
        <?php $content = ExampleData::getRandomName(); ?>
        <?php $date = ExampleData::getRandomDate(); ?>
        <?php $category = ExampleData::getRandomCategory(); ?>
        <?php $category2 = ExampleData::getRandomCategory(); ?>


        <li class="list-item"
            id="201409<?php echo str_pad((string)$i, 4, '0', STR_PAD_LEFT) ?>"
            data-search="<?php echo $content; ?> 201409<?php echo str_pad((string)$i, 4, '0', STR_PAD_LEFT) ?> <?php echo $category.' '. $category2 ;?>"
            data-filter="<?php echo $category . ' ' . $category2; ?>"
            data-sort-date="<?php echo $date->getTimestamp(); ?>"
            data-sort-name="<?php echo strtolower($content); ?>"
            >
            <a href="http://www.google.ch">
                <div class="left">
                    <p>Kategorie: <?php echo $category .', '. $category2; ?></p>

                    <p><?php echo $content; ?></p>
                </div>
                <div class="right">
                    <p>201409<?php echo str_pad((string)$i, 4, '0', STR_PAD_LEFT) ?></p>

                    <p>Datum: <?php echo $date->format('d.m.Y H:i:s'); ?></p>
                </div>
            </a>
        </li>
    <?php endfor; ?>
</ul>
<div class="pagination"></div>
<script>

    var list = new ListDotJs('list', [
            new ListDotJsPlugin.Selection({}),
            new ListDotJsPlugin.Searching(),
            new ListDotJsPlugin.Sorting(),
            new ListDotJsPlugin.Filtering(),
            new ListDotJsPlugin.Pagination({
                buttons: {
                    next: {
                        enabled: true,
                        text: 'vor'
                    },
                    prev: {
                        enabled: true,
                        text: 'zurück'
                    },
                    first: {
                        enabled: true,
                        text: 'erste Seite'
                    },
                    last: {
                        enabled: true,
                        text: 'letzte Seite'
                    }
                }

            }),
            new ListDotJsPlugin.Effects(),
            new ListDotJsPlugin.Ajax()
        ]
    );

</script>
</body>
</html>