// Namespace for Modules
var ListDotJsComponent = ListDotJsComponent || {};

/**
 * Item
 * @param element
 * @constructor
 */
ListDotJsComponent.Item = function (element) {

    /**
     * reference to this
     * @type {ListDotJsComponent.Item}
     */
    var self = this;

    /**
     * The Element
     * @type {HTMLElement}
     */
    var _element = element;

    /**
     * The Items Id
     * @type {null}
     */
    this.id = null;

    /**
     * The Items data
     * @type {{}}
     */
    this.data = {};

    /**
     * The Parent List
     * @type {ListDotJs}
     */
    this.parentList = false;

    /**
     * Set the HtmlElement
     * @param element
     */
    this.setElement = function (element) {

        _element = element;
    };

    /**
     * Get the HtmlElement
     * @returns {HTMLElement}
     */
    this.getElement = function () {

        if (!_element) {
            _element = document.getElementById(this.id);
        }
        return _element;
    };

    /**
     * Initialize the Item
     */
    var construct = function () {

        if (_element.hasAttribute('id')) {
            self.id = _element.getAttribute('id');
        } else {
            self.id = Math.random().toString(36).substr(2, 9);
            self.getElement().setAttribute('id', self.id);
        }

    };
    construct.call(this);
};


/**
 * Property for visibility
 * @type {boolean}
 */
ListDotJsComponent.Item.prototype.visible = true;


/**
 * check if visible
 * @returns {boolean}
 */
ListDotJsComponent.Item.prototype.isVisible = function () {

    return this.visible;
};

/**
 * set visible
 */
ListDotJsComponent.Item.prototype.show = function () {

    if (this.visible) {
        return;
    }
    this.visible = true;
    this.getElement().style.display = null;

};

/**
 * set not visible
 */
ListDotJsComponent.Item.prototype.hide = function () {

    if (!this.visible) {
        return;
    }
    this.visible = false;
    this.getElement().style.display = "none";
};

/**
 * toggle visibility
 */
ListDotJsComponent.Item.prototype.toggleVisibility = function () {

    if (this.visible) {
        this.hide();
    } else {
        this.show();
    }
};

