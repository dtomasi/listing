// Namespace for Modules
var ListDotJsComponent = ListDotJsComponent || {};


/**
 * Define the Container
 * @constructor Object
 */
ListDotJsComponent.ItemContainer = function (parent) {
    this.parent = parent;
    this.data = [];
    this.lastClicked = false;
};

/**
 * Count the Items
 * @returns {number}
 */
ListDotJsComponent.ItemContainer.prototype.count = function () {
    var count = 0, key;
    for (key in this.data) {
        if (this.data.hasOwnProperty(key)) {count++;}
    }
    return count;
};

/**
 * Clear the container
 */
ListDotJsComponent.ItemContainer.prototype.clear = function () {
    this.data = [];
};

/**
 * get the index of a item
 * @param item {ListDotJsComponent.Item}
 */
ListDotJsComponent.ItemContainer.prototype.indexOf = function (item) {

    var i = 0;
    /*jshint -W089 */
    for (var key in this.data) {
        if (this.data.hasOwnProperty(key) && this.data[key].id === item.id) {
            return i;
        }
        i++;
    }
    return false;
};

/**
 * Add a Item
 * @param item
 */
ListDotJsComponent.ItemContainer.prototype.add = function (item) {

    // do not continue if callback is not a ListItem
    if (!item instanceof ListDotJsComponent.Item) {
        return;
    }
    this.data.push(item);
};

/**
 * Remove a Item
 * @param element
 * @returns {*}
 */
ListDotJsComponent.ItemContainer.prototype.removeByElement = function (element) {

    return this.remove(this.getByElement(element));
};

/**
 * Remove Item by Id
 * @param item
 * @returns {*}
 */
ListDotJsComponent.ItemContainer.prototype.remove = function (item) {

    var key = this.data.indexOf(item);

    if (key){
        this.data[key].getElement().remove();
        delete this.data[key];
        return true;
    }
};

/**
 * Iterate through Items
 * @param callback
 */
ListDotJsComponent.ItemContainer.prototype.each = function (callback) {

    // loop through
    var key;
    for (key in this.data) {
        // check if key exists by convention
        if (this.data.hasOwnProperty(key)) {
            // store the return-value of the callback
            var result = callback(this.data[key], key);

            // check if the item is returned and overwrite if it has.
            if (result) {
                this.data[key] = result;
            }
        }
    }

};

/**
 * Get a Item
 * @param id
 * @returns {ListDotJsComponent.Item|boolean}
 */
ListDotJsComponent.ItemContainer.prototype.get = function (id) {

    var match = false;
    this.each(function(item){
        if (id === item.id) {
            match = item;
        }
    });
    return match;
};

/**
 * Get a Item
 * @param element
 * @returns {ListDotJsComponent.Item|boolean}
 */
ListDotJsComponent.ItemContainer.prototype.getByElement = function (element) {

    var id = element.getAttribute('id');
    if (!id) {
        return false;
    }
    return this.get(id);
};

/**
 * Select all
 */
ListDotJsComponent.ItemContainer.prototype.selectAll = function () {
    this.each(function (item) {
        item.select();
    });
};

/**
 * Deselect all
 */
ListDotJsComponent.ItemContainer.prototype.deselectAll = function () {
    this.each(function (item) {
        item.deselect();
    });
};

/**
 * show all
 */
ListDotJsComponent.ItemContainer.prototype.showAll = function () {
    this.each(function (item) {
        item.show();
    });
};

/**
 * hide all
 */
ListDotJsComponent.ItemContainer.prototype.hideAll = function () {
    this.each(function (item) {
        item.hide();
    });
};

var ListDotJsEvents = ListDotJsEvents || {};

/**
 * Show all Items
 * @type {*[]}
 */
ListDotJsEvents.showAll = [
    function () {
        this.items.showAll();
    }
];

/**
 * Show a Items
 * @type {*[]}
 */
ListDotJsEvents.showItem = [
    function () {
        this.items.getByElement(event.currentTarget).show();
    }
];

/**
 * Hide all Items
 * @type {*[]}
 */
ListDotJsEvents.hideAll = [
    function () {
        this.items.hideAll();
    }
];

/**
 * Hide a Item
 * @type {*[]}
 */
ListDotJsEvents.hideItem = [
    function () {
        this.items.getByElement(event.currentTarget).hide();
    }
];

/**
 * Toggle state of Visibility
 * @type {*[]}
 */
ListDotJsEvents.toggleVisibility = [
    function () {
        this.items.getByElement(event.currentTarget).toggleVisibility();
    }
];

/**
 * Remove a Item
 * @type {*[]}
 */
ListDotJsEvents.removeItem = [
    function () {
        this.items.removeByElement(event.currentTarget);
    }
];