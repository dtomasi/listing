var ListDotJsPlugin = ListDotJsPlugin || {};

/**
 * Effects for Listing
 * @constructor
 */
ListDotJsPlugin.Effects = function() {

    var self = this;

    this.name = 'effects';

    this.load = function() {};

    /**
     * Holds all easing options
     * @type {{linear: linear, quadratic: quadratic, swing: swing, circ: circ, back: back, bounce: bounce, elastic: elastic}}
     */
    this.easing = {

        linear: function (progress) {
            return progress;
        },
        quadratic: function (progress) {
            return Math.pow(progress, 2);
        },
        swing: function (progress) {
            return 0.5 - Math.cos(progress * Math.PI) / 2;
        },
        circ: function (progress) {
            return 1 - Math.sin(Math.acos(progress));
        },
        back: function (progress, x) {
            return Math.pow(progress, 2) * ((x + 1) * progress - x);
        },
        bounce: function (progress) {
            for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                if (progress >= (7 - 4 * a) / 11) {
                    return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
                }
            }
        },
        elastic: function (progress, x) {
            return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
        }
    };

    /**
     * Anmate the Element
     * @param options
     */
    this.animate = function (options) {
        var start = new Date;
        var id = setInterval(function () {
            var timePassed = new Date - start;
            var progress = timePassed / options.duration;
            if (progress > 1) {
                progress = 1;
            }
            options.progress = progress;
            var delta = options.delta(progress);
            options.step(delta);
            if (progress == 1) {
                clearInterval(id);
                if (options.complete) {
                    options.complete();
                }
            }
        }, options.delay || 10);
    };

    /**
     * Fade a Element out
     * @param element
     * @param options
     */
    this.fadeOut = function (element, options) {
        var to = 1;
        if ('undefined' == typeof options) {
            options = {};
            options.duration = 800;
            options.complete = false;
        }
        this.animate({
            duration: options.duration,
            delta: function (progress) {
                progress = this.progress;
                return self.easing.swing(progress);
            },
            complete: options.complete,
            step: function (delta) {
                element.style.opacity = to - delta;
            }
        });
    };

    /**
     * Fade a Element in
     * @param element
     * @param options
     */
    this.fadeIn = function (element, options) {
        var to = 0;
        if ('undefined' == typeof options) {
            options = {};
            options.duration = 800;
            options.complete = false;
        }

        this.animate({
            duration: options.duration || 800,
            delta: function (progress) {
                progress = this.progress;
                return self.easing.swing(progress);
            },
            complete: options.complete,
            step: function (delta) {
                element.style.opacity = to + delta;
            }
        });
    };
};