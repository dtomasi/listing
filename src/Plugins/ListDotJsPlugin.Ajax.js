/**
 * Created by dtomasi on 24.09.14.
 */

var ListDotJsPlugin = ListDotJsPlugin || {};

/**
 * Ajax Class for ListDotJs
 * @param options
 * @constructor
 */
ListDotJsPlugin.Ajax = function (options) {

    var self = this;

    this.options = {
        url: 'http://localhost/ajax',
        method: 'post',
        data: {},
        onSuccess: false,
        onError: false
    };

    /**
     *
     * @type {ListDotJs}
     */
    this.parent = false;

    this.name = 'ajax';

    var _xhr;

    var construct = function (options) {

        //noinspection JSPotentiallyInvalidUsageOfThis
        this.options = ListDotJsPlugin.mergeOptions(options,this.options);
    };

    this.load = function () {

        if (typeof XMLHttpRequest !== 'undefined') {
            _xhr = new XMLHttpRequest();
        }
        else {
            var versions = ["MSXML2.XmlHttp.5.0",
                "MSXML2.XmlHttp.4.0",
                "MSXML2.XmlHttp.3.0",
                "MSXML2.XmlHttp.2.0",
                "Microsoft.XmlHttp"];

            for (var i = 0, len = versions.length; i < len; i++) {
                try {
                    _xhr = new window.ActiveXObject(versions[i]);
                    break;
                }
                catch (e) {
                }
            }
        }

        if (!_xhr) {
            window.alert('Ajax is not supported');
        }
    };

    this.request = function (method, url, data, onSuccess, onError) {

        if ('undefined' === typeof method) {
            method = this.options.method;
        }

        if ('undefined' === typeof url) {
            url = this.options.url;
        }

        if ('undefined' === typeof data) {
            data = this.options.data;
        }

        _xhr.onreadystatechange = handler(onSuccess, onError);
        _xhr.open(method, url, true);
        _xhr.send(data);
    };

    var handler = function (onSuccess, onError) {
        // Request not ready
        if (_xhr.readyState < 4) {
            return;
        }

        // may be the url was not found
        if (_xhr.status !== 200) {
            if ('undefined' === typeof onError) {
                onError = self.options.onError;
            }
            onError(_xhr);
        }

        // all is well
        if (_xhr.readyState === 4) {
            if ('undefined' === typeof onSuccess) {
                onSuccess = self.options.onSuccess;
            }
            onSuccess(_xhr);
        }
    };

    construct.call(this,options);

};

ListDotJsPlugin.Ajax.prototype.get = function (url, data, onSuccess, onError) {

    this.request('get', url, data, onSuccess, onError);

};
ListDotJsPlugin.Ajax.prototype.post = function (url, data, onSuccess, onError) {

    this.request('post', url, data, onSuccess, onError);
};

ListDotJsPlugin.Ajax.prototype.put = function (url, data, onSuccess, onError) {

    this.request('put', url, data, onSuccess, onError);
};

ListDotJsPlugin.Ajax.prototype.patch = function (url, data, onSuccess, onError) {

    this.request('patch', url, data, onSuccess, onError);
};

ListDotJsPlugin.Ajax.prototype.httpDelete = function (url, data, onSuccess, onError) {

    this.request('delete', url, data, onSuccess, onError);
};