var ListDotJsPlugin = ListDotJsPlugin || {};

/**
 * Enables Filtering the List
 * @param options
 * @constructor
 */
ListDotJsPlugin.Filtering = function (options) {

    /**
     * The Name of this Plugin
     * @type {string}
     */
    this.name = 'Filtering';

    /**
     * The Default Settings
     * @type {{}}
     */
    this.options = {
        filterSelector: 'filter',
        filterAttribute: 'data-filter'
    };

    /**
     * The ParentList
     * @type {ListDotJsPlugin.Filtering}
     */
    this.parent = false;

    /**
     * Called on Constructing new Function-Object
     * @param options
     * @this {ListDotJsPlugin.Filtering}
     */
    var construct = function (options) {
        //noinspection JSPotentiallyInvalidUsageOfThis
        this.options = ListDotJsPlugin.mergeOptions(options,this.options);
    };

    /**
     * Called on Loading Plugin
     * @this {ListDotJsPlugin.Filtering}
     */
    this.load = function () {

        var self = this;

        var items = document.getElementsByClassName(this.options.filterSelector);

        if (items.length > 0) {
            /* jshint -W083 */
            for (var i = 0; i < items.length; i++) {
                items[i].addEventListener('click',function(){
                    self.filter.call(self);
                    self.parent.update();
                });
            }
        }

        this.parent.items.each(function(item){

            var filters = item.getElement().getAttribute(self.options.filterAttribute);
            if (filters) {
                item.data.filters =  filters.split(' ');
                item.data.filtered = false;
            }
        });

    };

    // Call Constructor
    construct.call(this, options);
};

/**
 * Filter-Function
 */
ListDotJsPlugin.Filtering.prototype.filter = function () {

    this.parent.items.showAll();

    var filterValues = event.currentTarget.getAttribute(this.options.filterAttribute);

    if (!filterValues || filterValues === 'all') {
        return;
    }

    filterValues = filterValues.split(' ');

    for (var i = 0; i < filterValues.length; i++) {
        /* jshint -W083 */
        this.parent.items.each(function(item){

            item.data.filtered = false;

            if (item.data.filters.indexOf(filterValues[i]) === -1) {
                item.hide();
                item.data.filtered = true;
            }
        });
    }
};