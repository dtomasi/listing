// used Namespaces
var ListDotJsPlugin = ListDotJsPlugin || {};

/**
 * Enables Pagination for List
 * @param options
 * @returns {ListDotJsPlugin.Pagination}
 * @constructor
 */
ListDotJsPlugin.Pagination = function (options) {

    /**
     * The Name
     * @type {string}
     */
    this.name = 'Pagination';

    /**
     * The Parent List
     * @type {ListDotJs}
     */
    this.parent = false;

    /**
     * The Plugin-options
     * @type {{selector: string, itemsPerPage: number, showLeft: number, showRight: number, buttons: {next: {enabled: boolean, text: string}, prev: {enabled: boolean, text: string}, first: {enabled: boolean, text: string}, last: {enabled: boolean, text: string}}}}
     */
    this.options = {
        selector: 'pagination',
        itemsPerPage: 10,
        showLeft: 2,
        showRight: 2,
        buttons: {
            next: {
                enabled: true,
                text: 'next'
            },
            prev: {
                enabled: true,
                text: 'prev'
            },
            first: {
                enabled: true,
                text: 'first'
            },
            last: {
                enabled: true,
                text: 'last'
            }
        }
    };

    /**
     * The Count of Pages
     * @type {number}
     */
    this.pageCount = 1;

    /**
     * The Current Page
     * @type {number}
     */
    this.currentPage = 1;


    this.paginations = null;
    /**
     * Constructor
     * @param options
     */
    var construct = function (options) {

        //noinspection JSPotentiallyInvalidUsageOfThis
        this.options = ListDotJsPlugin.mergeOptions(options,this.options);
    };

    /**
     * Called on Loading Plugin
     */
    this.load = function () {

        var self = this;

        this.update();

        this.parent.subscribeEvent('update', function () {
            self.update();
        });

    };

    /**
     * Add Page Data to Item
     */
    this.setPageData = function () {
        "use strict";

        var self = this,
            count = 0;

        self.pageCount = 1;

        this.parent.items.each(function (item) {

            if (count < self.options.itemsPerPage) {

                if (!item.data.filtered && !item.data.searched) {
                    item.getElement().setAttribute('data-page', self.pageCount.toString());
                    count++;
                }

            } else {

                if (!item.data.filtered && !item.data.searched) {
                    item.getElement().setAttribute('data-page', (self.pageCount++).toString());
                }
                count = 0;
            }
        });

    };

    /**
     * Build the Pagination
     */
    this.buildPagination = function () {

        var self = this;

        this.paginations = document.getElementsByClassName(this.options.selector);


        if (this.paginations && this.paginations.length > 0) {
            for (var x = 0; x < this.paginations.length; x++) {

                this.paginations[x].innerHTML = '';

                if (this.pageCount === 1) {
                    continue;
                }

                var list = document.createElement('ul');
                list.classList.add('list-pagination');

                if (this.currentPage !== 1) {

                    if (this.options.buttons.first.enabled === true) {

                        var first = document.createElement('li');
                        first.innerHTML = this.options.buttons.first.text;
                        first.addEventListener('click', function () {
                            self.first();
                        }, false);
                        list.appendChild(first);
                    }

                    if (this.options.buttons.prev.enabled === true) {

                        var prev = document.createElement('li');
                        prev.innerHTML = this.options.buttons.prev.text;
                        prev.addEventListener('click', function () {
                            self.prev();
                        }, false);
                        list.appendChild(prev);
                    }


                }


                if (this.currentPage > (this.options.showLeft + 1)) {
                    var dotsBefore = document.createElement('li');
                    dotsBefore.innerHTML = '...';
                    list.appendChild(dotsBefore);
                }

                for (var i = 1; i <= this.pageCount; i++) {


                    if (i < (this.currentPage - this.options.showLeft)) {
                        continue;
                    }

                    if (i > (this.currentPage + this.options.showRight)) {
                        continue;
                    }

                    var item = document.createElement('li');
                    item.innerHTML = i.toString();

                    if (i === this.currentPage) {
                        item.classList.add('current');
                    } else {
                        item.addEventListener('click', function (event) {
                            self.jump(parseInt(event.currentTarget.innerHTML));
                        }, false);
                    }

                    list.appendChild(item);

                }

                if (this.currentPage < (this.pageCount - this.options.showRight )) {
                    var dotsAfter = document.createElement('li');
                    dotsAfter.innerHTML = '...';
                    list.appendChild(dotsAfter);
                }

                if (this.currentPage !== this.pageCount) {

                    if (this.options.buttons.next.enabled === true) {

                        var next = document.createElement('li');
                        next.innerHTML = this.options.buttons.next.text;
                        next.addEventListener('click', function () {
                            self.next();
                        }, false);
                        list.appendChild(next);
                    }


                    if (this.options.buttons.last.enabled === true) {

                        var last = document.createElement('li');
                        last.innerHTML = this.options.buttons.last.text;
                        last.addEventListener('click', function () {
                            self.last();
                        }, false);
                        list.appendChild(last);
                    }


                }

                this.paginations[x].appendChild(list);

            }
        }
    };

    /**
     * Run the Constructor
     */
    construct.call(this, options);

    /**
     * Return the Object itself
     */
    return this;
};

/**
 * Update the Pagination
 */
ListDotJsPlugin.Pagination.prototype.update = function () {

    var self = this;

    this.setPageData();
    this.buildPagination();

    this.parent.items.each(function (item) {
        var itemPage = parseInt(item.getElement().dataset.page);

        if (itemPage !== self.currentPage) {
            item.hide();
        }

        if (itemPage === self.currentPage) {
            item.show();
        }
    });

};

/**
 * jump to a specific page
 * @param pageId
 */
ListDotJsPlugin.Pagination.prototype.jump = function (pageId) {
    if (pageId <= this.pageCount) {
        this.currentPage = pageId;
        this.update();
    }
};

/**
 * jump to next page
 */
ListDotJsPlugin.Pagination.prototype.next = function () {
    if (this.currentPage < this.pageCount) {
        this.currentPage++;
        this.update();
    }
};

/**
 * jump to prev page
 */
ListDotJsPlugin.Pagination.prototype.prev = function () {
    if (this.currentPage > 1) {
        this.currentPage--;
        this.update();
    }
};

/**
 * jump to first page
 */
ListDotJsPlugin.Pagination.prototype.first = function () {
    this.currentPage = 1;
    this.update();
};

/**
 * jump to last page
 */
ListDotJsPlugin.Pagination.prototype.last = function () {
    this.currentPage = this.pageCount;
    this.update();
};