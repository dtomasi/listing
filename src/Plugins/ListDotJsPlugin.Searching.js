var ListDotJsPlugin = ListDotJsPlugin || {};

/**
 * Enables searching via a text input field
 * @param options {{}|undefined}
 * @constructor
 */
ListDotJsPlugin.Searching = function(options) {

    /**
     * The Plugins Name
     * @type {string}
     */
    this.name = 'Searching';

    /**
     * The Default-Options
     * @type {{searchInputId: string, dataAttribute: string, separator: string, minStringLength: number, caseSensitive: boolean}}
     */
    this.options = {
        searchInputId: 'list-search',
        dataAttribute: 'data-search',
        separator: " ",
        minStringLength: 1,
        caseSensitive: false
    };

    /**
     * The Input Field
     * @type {boolean}
     */
    this.input = false;

    /**
     * The Constructor
     * @param options
     */
    var construct = function (options) {

        //noinspection JSPotentiallyInvalidUsageOfThis
        this.options = ListDotJsPlugin.mergeOptions(options,this.options);
    };

    /**
     * Called on Loading Plugin
     */
    this.load = function(){

        var self = this;

        this.input = document.getElementById(this.options.searchInputId);

        if (this.input) {
            this.input.addEventListener("keyup", function (event) {

                if (event.target.value.length >= self.options.minStringLength) {
                    self.searchItems(event.target.value);
                    // @todo trigger a update Event, so that pagination is up to date
                }

                if (event.target.value.length < self.options.minStringLength) {
                    self.parent.items.showAll();
                    // @todo trigger a update Event, so that pagination is up to date
                }

            }, false);
        }

        this.parent.items.each(function(item){
            var element = item.getElement();

            var keywords = element.getAttribute(self.options.dataAttribute);
            if (keywords) {
                //keywords = keywords.split(self.options.separator);
                item.data.keywords = keywords;
                item.data.searched = false;
            }
            return item;
        });


    };

    construct.call(this,options);
};

/**
 * The Search Function
 * @param string
 */
ListDotJsPlugin.Searching.prototype.searchItems = function(string) {

    // show all items
    this.parent.items.showAll();

    // Split the input of the Search-Box to single search-words
    var arrSearchWords = string.split(' ');

    // Find items for all search-words
    for (var i = 0; i < arrSearchWords.length; i++) {

        var stringFragment;
        // build regex based on case-sensibility
        if (this.options.caseSensitive === false) {
            stringFragment = new RegExp(arrSearchWords[i], "i");
        } else {
            stringFragment = arrSearchWords[i];
        }

        // iterate through the items
        /* jshint -W083 */
        this.parent.items.each(function (item) {

            var keywords = item.data.keywords;

            // if not matching -> hide the item
            if (keywords.search(stringFragment) < 0) {
                item.hide();
                item.data.searched = true;
            }
        });
    }
};