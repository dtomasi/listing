var ListDotJsPlugin = ListDotJsPlugin || {};
var ListDotJsEvents = ListDotJsEvents || {};
var ListDotJsComponent = ListDotJsComponent || {};

/**
 * Enables selecting Item by Click or Multiple with SHIFT
 * @param {Object|undefined} options
 *
 * @constructor
 */
ListDotJsPlugin.Selection = function (options) {

    /**
     * The Name of this Plugin
     * @type {string}
     */
    this.name = 'Selection';

    /**
     * The Current Options
     * @type {{}}
     */
    this.options = {
        autoActivate: true,
        selectOnlyWithAltKey: true,
        multipleWithShiftKey: true,
        deleteSelectedItemsWithKey: true
    };

    /**
     * The Parent List
     * @type {ListDotJsPlugin.Selection}
     */
    this.parent = false;

    /**
     * is Active
     * @type {boolean}
     */
    this.active = false;

    /**
     * Called on Constructing new Function-Object
     * @param options
     */
    var construct = function (options) {

        //noinspection JSPotentiallyInvalidUsageOfThis
        this.options = ListDotJsPlugin.mergeOptions(options,this.options);
    };

    /**
     * Called on Loading the Plugin
     */
    this.load = function () {

        var self = this;

        setEvents();

        if (this.options.autoActivate) {
            this.activate();
        }

        if (this.options.deleteSelectedItemsWithKey) {
            window.addEventListener('keyup', function (event) {
                "use strict";

                if (self.parent.items.getSelectedItemsCount() > 0) {
                    if (event.keyCode === 8 || event.keyCode === 46) {

                        var result = window.confirm("Want to delete " + self.parent.items.getSelectedItemsCount() + " Objects?");
                        if (result === true) {
                            self.parent.items.removeSelectedItems();
                        }
                    }
                }
            }, false);
        }
    };

    /**
     * Define all Events for this Plugin
     */
    var setEvents = function () {

        /**
         * Select a Item
         * @type {*[]}
         */
        ListDotJsEvents.selectItem = [
            function () {
                this.items.getByElement(event.currentTarget).select();
            }
        ];

        /**
         * Deselect a Item
         * @type {*[]}
         */
        ListDotJsEvents.deselectItem = [
            function () {
                this.items.getByElement(event.currentTarget).deselect();
            }
        ];

        /**
         * Toggle Selection-State
         * @type {*[]}
         */
        ListDotJsEvents.toggleSelectItem = [
            function () {
                this.items.getByElement(event.currentTarget).toggleSelect();
            }
        ];

        /**
         * Select all Items
         * @type {*[]}
         */
        ListDotJsEvents.selectAll = [
            function () {
                this.items.selectAll();

            }
        ];

        /**
         * Deselect all Items
         * @type {*[]}
         */
        ListDotJsEvents.deselectAll = [
            function () {
                this.items.deselectAll();
            }
        ];

        /**
         * Remove selected Items
         * @type {*[]}
         */
        ListDotJsEvents.removeSelected = [
            function () {
                this.items.removeSelectedItems();
            }
        ];

    };

    /**
     * @this {ListDotJsPlugin.Selection}
     */
    this.clickHandler = function () {

        if (!this.active) {
            return;
        }

        if (this.options.selectOnlyWithAltKey) {

            if (event.altKey) {
                this.parent.items.getByElement(event.currentTarget).toggleSelect();
                event.preventDefault();
            }

            if (event.shiftKey) {
                this.multiselect();
                event.preventDefault();
            }

        } else {

            if (event.shiftKey) {
                this.multiselect();
            } else {
                this.parent.items.getByElement(event.currentTarget).toggleSelect();
            }
        }

        if (this.parent.items.getSelectedItemsCount() > 0) {
            event.preventDefault();
        }

    };


    construct.call(this, options);
};

/**
 * Activate Selection-Mode
 */
ListDotJsPlugin.Selection.prototype.activate = function () {

    var self = this;
    this.active = true;
    this.parent.items.each(function (item) {
        item.getElement().addEventListener('click', function () {
            self.clickHandler.call(self);
        }, false);
    });

};

/**
 * Deactivate Selection-Mode
 */
ListDotJsPlugin.Selection.prototype.deactivate = function () {

    var self = this;
    this.active = false;
    this.parent.items.each(function (item) {
        item.deselect();
        item.getElement().removeEventListener('click', function () {
            self.clickHandler.call(self);
        }, false);
    });
};

/**
 * Select Multiple items with Shift-Key
 */
ListDotJsPlugin.Selection.prototype.multiselect = function () {
    "use strict";

    var self = this;

    if (this.parent.items.lastClicked) {
        var start = this.parent.items.indexOf(this.parent.items.getByElement(event.currentTarget));
        var end = this.parent.items.indexOf(this.parent.items.lastClicked);

        if (start && end) {
            this.parent.items.each(function (item) {
                var index = self.parent.items.indexOf(item);
                if (index > Math.min(start, end) && index <= Math.max(start, end)) {
                    item.toggleSelect();
                }
            });
        }
    }

};

/**
 * Extends the Container
 * @returns {Array}
 */
ListDotJsComponent.ItemContainer.prototype.getSelectedItems = function () {
    "use strict";

    var selected = [];
    this.each(function (item) {
        if (item.selected) {
            selected.push(item);
        }
    });
    return selected;

};

/**
 * Extends the Container
 * @returns {int}
 */
ListDotJsComponent.ItemContainer.prototype.getSelectedItemsCount = function () {
    "use strict";
    return this.getSelectedItems().length;
};

/**
 * Extends the Container
 * @returns {int}
 */
ListDotJsComponent.ItemContainer.prototype.removeSelectedItems = function () {
    "use strict";
    var self = this;
    this.each(function (item) {
        if (item.selected) {
            self.remove(item);
        }
    });
    this.parent.update();
};

/**
 * property for selected
 * @type {boolean}
 */
ListDotJsComponent.Item.prototype.selected = false;


/**
 * check if selected
 * @returns {boolean}
 */
ListDotJsComponent.Item.prototype.isSelected = function () {

    return this.selected;
};


/**
 * set selected
 */
ListDotJsComponent.Item.prototype.select = function () {

    this.selected = true;
    this.getElement().classList.add('selected');

    this.parentList.items.lastClicked = this;
};


/**
 * set not selected
 */
ListDotJsComponent.Item.prototype.deselect = function () {

    this.selected = false;
    this.getElement().classList.remove('selected');

    this.parentList.items.lastClicked = this;
};


/**
 * toggle selected
 */
ListDotJsComponent.Item.prototype.toggleSelect = function () {

    if (this.selected) {
        this.deselect();
    } else {
        this.select();
    }
};


