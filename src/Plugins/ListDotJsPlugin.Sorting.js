// used Namespaces
var ListDotJsPlugin = ListDotJsPlugin || {};
var ListDotJsEvents = ListDotJsEvents || {};
var ListDotJsComponent = ListDotJsComponent || {};

/**
 * Enables Sorting the List by Values
 * @param options {{}|undefined}
 * @constructor
 */
ListDotJsPlugin.Sorting = function (options) {

    /**
     * The Name of this Plugin
     * @type {string}
     */
    this.name = 'Sorting';

    /**
     * The Current Options
     * @type {{}}
     */
    this.options = {
        sortSelector: 'sort',
        sortAttribute: 'data-sort',
        sortOrderAttribute: 'data-order'
    };

    /**
     * The Parent List
     * @type {ListDotJs}
     */
    this.parent = false;

    /**
     * The Constructor
     * @param options
     */
    var construct = function (options) {

        //noinspection JSPotentiallyInvalidUsageOfThis
        this.options = ListDotJsPlugin.mergeOptions(options,this.options);
    };

    /**
     * Called on Loading Plugin
     * @this {ListDotJsPlugin.Sorting}
     */
    this.load = function () {

        var self = this;

        // Bind ClickEvent to .sort Elements
        var sortItems = document.getElementsByClassName(this.options.sortSelector);
        if (sortItems.length > 0) {
            /* jshint -W083 */
            for (var i = 0; i < sortItems.length; i++) {
                sortItems[i].addEventListener('click', function () {
                    "use strict";
                    var sortKey = this.getAttribute(self.options.sortAttribute);

                    if (sortKey) {
                        var sortOrder = this.getAttribute(self.options.sortOrderAttribute);
                        if (!sortOrder) {
                            sortOrder = 'asc';
                        }

                        var sortType;
                        if (sortKey.match('/^\d+$/')) {
                            sortType = 'num';
                        }
                        if (sortKey.match('/^[a-z0-9]+$/i')) {
                            sortType = 'alphanum';
                        }
                    }

                    self.parent.items.sort(sortKey, sortType, sortOrder.toLowerCase());

                }, false);
            }
        }

        // read the sort-Data from Items
        this.parent.items.each(function (item) {
            "use strict";
            var element = item.getElement();

            item.data.sorting = {};
            for (var key in element.dataset) {
                if (element.dataset.hasOwnProperty(key) && key.slice(0, 4) === 'sort') {
                    item.data.sorting[key.replace('sort', '').toLowerCase()] = element.dataset[key];
                }
            }
            // add id for Sorting;
            item.data.sorting.id = parseInt(item.id);
        });

    };

    // call constructor-closure
    construct.call(this, options);

    return this;
};



/**
 * Sort Container-Items
 * @param sortKey
 * @param sortType
 * @param sortOrder
 */
ListDotJsComponent.ItemContainer.prototype.sort = function (sortKey, sortType, sortOrder) {
    "use strict";

    if (sortOrder === 'desc') {

        this.data.sort(function (a, b) {

            var aValue = a.data.sorting[sortKey];

            if (!isNaN(parseFloat(aValue)) && isFinite(aValue)) {
                return b.data.sorting[sortKey] - aValue;
            } else {

                if (a.data.sorting[sortKey] > b.data.sorting[sortKey]) {
                    return -1;
                }
                if (a.data.sorting[sortKey] < b.data.sorting[sortKey]) {
                    return 1;
                }
                return 0;
            }

        });

    } else {

        this.data.sort(function (a, b) {

            var bValue = b.data.sorting[sortKey];

            if (!isNaN(parseFloat(bValue)) && isFinite(bValue)) {
                return a.data.sorting[sortKey] - bValue;
            } else {

                if (b.data.sorting[sortKey] > a.data.sorting[sortKey]) {
                    return -1;
                }
                if (b.data.sorting[sortKey] < a.data.sorting[sortKey]) {
                    return 1;
                }
                return 0;
            }
        });

    }

    this.parent.update();
};