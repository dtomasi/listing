// used Namespaces
var ListDotJsPlugin = ListDotJsPlugin || {};

/**
 * Skeleton for a Plugin
 * @param options
 * @returns {ListDotJsPlugin.Skeleton}
 * @constructor
 */
ListDotJsPlugin.Skeleton = function(options) {

    /**
     * The Name
     * @type {string}
     */
    this.name = 'Skeleton';

    /**
     * The Parent List
     * @type {ListDotJs}
     */
    this.parent = false;

    /**
     * Default options
     * @type {{}}
     */
    this.options = {};

    /**
     * Constructor
     * @param options
     */
    var construct = function(options) {
        //noinspection JSPotentiallyInvalidUsageOfThis
        this.options = ListDotJsPlugin.mergeOptions(options,this.options);
    };

    /**
     * Called on Loading Plugin
     */
    this.load = function() {};

    /**
     * Run the Constructor
     */
    construct.call(this,options);
};
