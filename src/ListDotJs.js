// used Namespaces
var ListDotJsEvents = ListDotJsEvents || {};
var ListDotJsComponent = ListDotJsComponent || {};
var ListDotJsPlugin = ListDotJsPlugin || {};

/**
 * ListDotJs
 * @param containerId string
 * @param plugins []
 * @constructor
 */
function ListDotJs(containerId, plugins) {

    "use strict";

    /**
     * reference to this
     * @type {ListDotJs}
     */
    var self = this;

    /***
     * Store Plugins
     * @type {[]|boolean}
     * @private
     */
    var _plugins = plugins || false;

    /**
     * the Options container
     * @type {{}}
     * @private
     */
    var _options = {};

    /**
     * Holds the Container
     * @type {HTMLElement}
     */
    this.container = null;

    /**
     * Holds the Items as Objects in a Container
     * @type {ListDotJsComponent.ItemContainer}
     */
    this.items = false;

    /**
     * set a option
     * @param key
     * @param value
     */
    this.setOption = function (key, value) {
        if (!_options.hasOwnProperty(key)) {
            _options[key] = value;
        }
    };

    /**
     * get a option
     * @param key
     * @returns {*}
     */
    this.getOption = function (key) {
        if (_options.hasOwnProperty(key)) {
            return _options[key];
        }
        return false;
    };

    /**
     * get a option
     * @param key
     * @returns {*}
     */
    this.getPlugin = function (key) {

        for (var i = 0; i < _plugins.length; i++) {
            if (_plugins[i].name === key) {
                return _plugins[i];
            }
        }
        return false;
    };

    /**
     * Initialize
     * @param containerId
     */
    var construct = function (containerId) {

        // get the Container
        self.container = window.document.getElementById(containerId);

        // No container found ->
        if (!self.container) {
            window.alert('Container with id "' + containerId + '" could not be found');
        }

        // Construct ItemContainer
        self.items = new ListDotJsComponent.ItemContainer(self);

        // Load Items from Html-Container to ItemContainer
        self.iterateContainer(function (element) {
            var item = new ListDotJsComponent.Item(element);
            item.parentList = self;
            self.items.add(item);
        });

        // Load the plugins -> important!! Load Plugins after building ItemContainer and Items
        if (_plugins.length > 0) {
            for (var i = 0; i < _plugins.length; i++) {
                _plugins[i].parent = self;
                _plugins[i].load();
            }
        }
    };

    construct.call(this, containerId);
}

/**
 * Serve a Merge options-method for Plugins
 * @param defaultOptions
 * @param options
 * @returns {{}}
 */
ListDotJsPlugin.mergeOptions = function (options, defaultOptions) {

    if (typeof options === 'undefined') {
        return defaultOptions;
    }

    var result = {};
    for (i in options) {
        result[i] = options[i];
        if ((i in defaultOptions) && (typeof options[i] === "object") && (i !== null)) {
            result[i] = this.mergeOptions(options[i], defaultOptions[i]);
        }
    }
    for (i in defaultOptions) {
        if (i in result) { //conflict
            continue;
        }
        result[i] = defaultOptions[i];
    }
    return result;
};

/**
 * iterate through all children of HTML-Container
 * @param callback
 */
ListDotJs.prototype.iterateContainer = function (callback) {

    'use strict';

    if (this.container.children.length > 0) {
        var children = this.container.children;
        for (var i = 0; i < children.length; i++) {
            children[i] = callback(children[i]);
        }
    }
};

/**
 * Update the List
 */
ListDotJs.prototype.update = function () {

    'use strict';

    var self = this;
    this.container.innerHTML = '';

    this.items.each(function (item) {
        self.container.appendChild(item.getElement());
    });

    this.trigger('update');
};

/**
 * Fire a event
 * @param eventName
 * @param args
 * @param callback
 */
ListDotJs.prototype.trigger = function (eventName, args, callback) {

    var self = this;

    if (!ListDotJsEvents.hasOwnProperty(eventName)) {
        return;
    }

    args = Array.prototype.slice.call(arguments, 0);
    args.shift();

    ListDotJsEvents[eventName].forEach(function (item) {
        item.apply(self, args);

        var callback = arguments[arguments.length - 1];
        if ('function' === typeof callback) {
            callback();
        }
    });
};

/**
 * Proxy for EventHandler
 * @param eventName
 * @param callback
 */
ListDotJs.prototype.subscribeEvent = function (eventName, callback) {

    'use strict';

    if (!ListDotJsEvents.hasOwnProperty(eventName)) {
        ListDotJsEvents[eventName] = [];
    }
    ListDotJsEvents[eventName].push(callback);
};

/**
 * Proxy for EventHandler
 * @param eventName
 * @param callback
 */
ListDotJs.prototype.unsubscribeEvent = function (eventName, callback) {

    'use strict';

    if (!ListDotJsEvents.hasOwnProperty(eventName)) {
        return;
    }

    ListDotJsEvents[eventName] = ListDotJsEvents[eventName].filter(
        function (item) {
            if (item !== callback) {
                return item;
            }
            return false;
        }
    );
};

/**
 * Predefine update-event
 * @type {*[]}
 */
ListDotJsEvents.update = [];




